# **Project initialization**
- Edit .lando.yml to adapt to your env.
- `cd www` 
- `lando composer install` 
- `lando drush site-install ywd_profile_base -y --existing-config`
- `lando drupal init --no-interaction --override --quiet`
- _If local env_
    - `lando drush upwd admin admin`

That's it ! :)
    
