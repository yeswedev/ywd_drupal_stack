#!/bin/bash

cat << "EOF"
+----------------------------------------+
   __   ___   _  _____  ____
  / /  / _ | / |/ / _ \/ __ \
 / /__/ __ |/    / // / /_/ /
/____/_/ |_/_/|_/____/\____/

Local Build for Drupal 8 !

By Insign
+----------------------------------------+
EOF

# Start
start=$(date +"%s")

# Build commands.
composer install

drush cache-rebuild -l lamberet -y
drush updatedb -l lamberet -y
drush config-import -l lamberet -n
drush config-split:import -l lamberet -y

# drush entity-updates -l lamberet -y

echo "+----------------------------------------+"
end=$(date +"%s")
difftimelps=$(($end-$start))
echo "$(($difftimelps / 60)) minutes $(($difftimelps % 60)) seconds elapsed time."
echo "+----------------------------------------+"